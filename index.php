<?php
declare(strict_types=1);
require 'src/tree.php';
try {
    process();
} catch (\Exception $e) {
    file_put_contents('exception.log', $e->getMessage(), FILE_APPEND);
}
/**
 * @param int $items
 * @param int $from
 * @param int $to
 *
 * @return array
 * @throws Exception
 */
function generateDataset(int $items = 100, int $from = 0, int $to = 1000)
{
    $list = [];
    for ($i = 0; $i < $items; $i++) {
        $list[] = random_int($from, $to);
    }
    sort($list);

    return $list;
}

/**
 * @return void
 * @throws Exception
 */
function process()
{
    $case = 'find';
//     $case = 'insert';
//     $case = 'delete';
    $tree = new Tree;
    for ($i = 1; $i <= 50; $i++) {
        $multpl = 10;
        $items = 10 * $i * $multpl;
        $from = $i * $multpl;
        $to = 500 * $i * $multpl;
        $dataset = generateDataset($items, $from, $to);
        switch ($case) {
            case 'insert':
                $start = microtime(true);
                $tree->balance($dataset);
                $delta = microtime(true) - $start;
                print_r($delta . "\n");
                break;
            case 'find':
                $tree->balance($dataset);
                $start = microtime(true);
                foreach ($dataset as $value) {
                    $tree->find($value);
                }
                $delta = microtime(true) - $start;
                print_r($delta . "\n");
                break;
            case 'delete':
                $tree->balance($dataset);
                $start = microtime(true);
                foreach ($dataset as $value) {
                    $tree->delete($value);
                }
                $delta = microtime(true) - $start;
                print_r($delta . "\n");
                $tree->balance($dataset);
        }
    }
}